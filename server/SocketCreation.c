//
// Created by ana on 30/04/19.
//
#include "SocketCreation.h"

int createSocket(struct addrinfo *addr) {
    int sock;

    sock = socket(addr->ai_family, addr->ai_socktype, 0);
    if (sock < 0) {
        printf("socket error: %s\n", strerror(errno));
        return 1;
    }

    int optval = 1;
    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
    signal(SIGPIPE, SIG_IGN);
    return sock;
}

int liaisonPortServer(struct addrinfo *addr, int sock) {
    int status;

    status = bind(sock, addr->ai_addr, addr->ai_addrlen);
    if (status != 0) {
        printf("bind error: %s\n", strerror(errno));
        return -1;
    }
    return 0;
}