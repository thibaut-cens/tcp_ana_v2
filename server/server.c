//
// Created by ana on 28/04/19.
//
#include "server.h"

int serverConnection(int sock) {
    //mode listen//
    int status = listen(sock, 1024);
    if (status != 0) {
        printf("listen error: %s\n", strerror(errno));
        return -1;
    }
    return sock;
    //acceptation d'une connexion //
}

int acceptConnection(int sock) {

    int sock_accept = accept(sock, NULL, NULL);
    if (sock_accept < 0) {
        printf("accept error: %s\n", strerror(errno));
    }
    return sock_accept;
}

int recieveData(int sock) {
    int readen = 0;

    do {
        char buffer[1024] = {0};
        readen = recv(sock, buffer, 1024, 0);
        if (readen < 0) {
            printf("read error: %s\n", strerror(errno));
            return -1;
        }
        if (readen == 0)
            return 1;
        char response[1030] = "> ";
        strncat(response, buffer, strlen(buffer));
        send(sock, response,strlen(response), 0);
    } while (readen > 0);

    if (readen < 0) {
        printf("recv error: %s\n", strerror(errno));
        return -1;
    }
    return 0;
}

char *get_ip_str(const struct sockaddr *sa, char *s, size_t maxlen)
{
    switch(sa->sa_family) {
        case AF_INET:
            inet_ntop(AF_INET, &(((struct sockaddr_in *)sa)->sin_addr),
                      s, maxlen);
            break;

        case AF_INET6:
            inet_ntop(AF_INET6, &(((struct sockaddr_in6 *)sa)->sin6_addr),
                      s, maxlen);
            break;

        default:
            strncpy(s, "Unknown AF", maxlen);
            return NULL;
    }

    return s;
}