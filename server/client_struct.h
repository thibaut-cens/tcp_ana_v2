#ifndef CLIENT_STRUCT
#define CLIENT_STRUCT

#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

#define NBCLIENTS 2

typedef struct client_data_s {
    int used;
    int sock;
    pthread_t thread;
}client_data;

/**
 * @brief Force stop a client
 * @param client Pointer to the client to stop
 * @return return 0 on success, -1 otherwise
 */
int stop_client(client_data *client);

/**
 * @brief Get next free client in an array of client
 * @param array Array of client in which the search is done
 * @param size Size of the search
 * @return On success return a pointer on the unused client, return NULL
 * otherwise
 */
client_data *get_next_free(client_data *array, const unsigned size);

#endif

