#include "client_struct.h"

client_data *get_next_free(client_data *array, const unsigned size) {
    for (int i = 0; i < size; i++) {
        if (array[i].used == 0)
            return &(array[i]);
    }
    return NULL;
}

int stop_client(client_data *client) {
    if (client->used) {
        pthread_cancel(client->thread);
        if (close(client->sock) != 0) {
            printf("socket %d: close error: %s\n",
                   client->sock,
                   strerror(errno));
            return -1;
        }
    }
    return 0;
}