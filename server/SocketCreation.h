//
// Created by ana on 05/05/19.
//

#ifndef SERPROJET_SOCKETCREATION_H
#define SERPROJET_SOCKETCREATION_H

#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>
#include <string.h>
#include <signal.h>


int createSocket(struct addrinfo* addr);

int liaisonPortServer(struct addrinfo*,int sock);


#endif //SERPROJET_SOCKETCREATION_H
