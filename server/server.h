//
// Created by ana on 05/05/19.
//

#ifndef SERPROJET_SERVER_H
#define SERPROJET_SERVER_H

#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>
#include <string.h>
#include <libnet.h>

int serverConnection(int sock);

int acceptConnection(int sock);

int recieveData(int sock);

char *get_ip_str(const struct sockaddr *sa, char *s, size_t maxlen);

#endif //SERPROJET_SERVER_H
