//
// Created by ana on 28/04/19.
//

#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pthread.h>

#include "SocketCreation.h"
#include "server.h"
#include "client_struct.h"

struct sockaddr *address;

int stop = 0;
int connections = 0;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void sigint_handler(int dummy) {
    printf("Closing server...\n");
    stop = 1;
}

/**
 * \brief Force all clients to stop
 * @param clients Array of clients to stop
 * @param size Size of the array
 * @return Return 0 on succes, -1 otherwise
 */

int stop_all_clients(client_data *clients, unsigned size) {
    int retcode = 0;
    for (unsigned i = 0; i < size; i++) {
        int status = 0;
        if ((status = stop_client(clients + i)) != 0)
            retcode = -1;
    }
    return retcode;
}

/**
 * @brief Prepare a socket for it's server job and then free memory
 * @return Return a value greater or equal to zero corresponding to the socket
 * descriptor on success. On failure return an error code lower than zero.
 */
int prepare_socket(char port[]) {
    struct addrinfo hints = {};
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    struct addrinfo *res;
    int status = 0;
    status = getaddrinfo(NULL, port, &hints, &res);
    if (status != 0) {
        printf("getaddrinfo error: %s\n", gai_strerror(status));
        return -1;
    }

    int sock = createSocket(res);
    if (sock == -1)
        return -2;
    int optval = 1;
    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
    if (liaisonPortServer(res, sock) == -1)
        return -3;

    if (serverConnection(sock) < 0)
        return -4;

    freeaddrinfo(res);

    return sock;
}

void *thread_entry_point(void *void_arg) {
    client_data *client = (client_data *) void_arg;
    if (client == NULL)
        pthread_exit(NULL);

    int status = recieveData(client->sock);

    //Thread ewiting
    pthread_mutex_lock(&mutex);
    shutdown(client->sock, 2);
    client->sock = 0;

    client->used = 0;
    connections--;
    pthread_mutex_unlock(&mutex);
    pthread_exit(NULL);
}

int manage_accept_thread_creation(int sock_accept, client_data clients[]) {
    pthread_mutex_lock(&mutex);
    client_data *client = get_next_free(clients, NBCLIENTS);
    if (client == NULL || connections == NBCLIENTS) {
        //If we reach max clients we say to the client that he can't
        // connect
        printf("%s (%d/%d)\n", "Maximum number of clients reached.",
               connections, NBCLIENTS);
        if (send(sock_accept,
                 "Maximum number of clients reached.\n",
                 36,
                 0) != 0)
            printf("Socket %d: send error: %s\n", sock_accept,
                   strerror(errno));
        shutdown(sock_accept, 2);
        pthread_mutex_unlock(&mutex);
        return 1;
    }
    client->used = 1;
    client->sock = sock_accept;
    connections++;
    pthread_create(&client->thread, NULL, thread_entry_point,
                   client);
    pthread_detach(client->thread);
    pthread_mutex_unlock(&mutex);
    return 0;
}

/**
 * @brief Main loop of the server management
 * @param sock Server socket
 * @return return 0 on success, a values different from 0 otherwise.
 */
int run_server(int sock) {
    client_data clients[NBCLIENTS] = {0};
    connections = 0;
    int ret = 0;

    while (!stop) {
        //Whe want to check we have a connection waiting, before calling to
        //a blocking accept
        struct timeval tv = {};
        tv.tv_sec = 3;

        fd_set readfds;
        FD_ZERO(&readfds);
        FD_SET(sock, &readfds);

        select(sock + 1, &readfds, NULL, NULL, &tv);

        if (FD_ISSET(sock, &readfds) && stop != 1) {
            struct sockaddr_storage from;
            socklen_t from_size = sizeof(from);
            int sock_accept = accept(sock, (struct sockaddr *) &from,
                                     &from_size);
            if (sock_accept < 0) {
                ret = 5;
                break;
            } else {
                char from_str[256];
                if (getnameinfo((struct sockaddr *) &from, from_size,
                                 from_str, sizeof(from_str),
                                 NULL, 0,
                                 0) != 0)
                    get_ip_str((struct sockaddr *) &from, from_str, 256);
                printf("Recieved connection from: %s\n", from_str);
                manage_accept_thread_creation(sock_accept, clients);
            }
        }
    }

    if (connections != 0) {
        int status;
        if ((status = stop_all_clients(clients, NBCLIENTS)))
            return status;
    }

    return ret;
}

int main(int argc, char* argv[]) {
    signal(SIGPIPE, SIG_IGN);
    signal(SIGINT, sigint_handler);

    if (argc != 2) {
        printf("%s <port>\n", argv[0]);
        return 1;
    }

    int sock = prepare_socket(argv[1]);
    if (sock < 0)
        return -sock;

    int ret = 0;
    ret = run_server(sock);

    shutdown(sock, 2);

    return ret;
}